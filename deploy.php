<?php
/**
This is an example implementation, rename to something sensible, like hook/index/post.php
**/

error_reporting(E_ALL);
ini_set('display_errors','On');

//Include deployment class
require_once 'class.php';

$config = @file_get_contents(dirname(__FILE__).'/config.json');
if(!$config){
	echo 'No config file config.json found';
	exit;
}

$config = json_decode($config, true);
if(!$config){
	echo 'Unable to parse config.json, syntax errors';
	exit;
}

/*
 * Define the json config according to this structure
 *

$config = array(
	'repo' : '', //Enter your repo name here
	'branch' : 'master', //Enter your branch name here
	'validate_branch' : true, //To validate the branch matches a committed branch set to true
	'hosts' => [
		'63.246.22.222' //Enter valid host name or IP addresses if you wish to validate. This IP is bitbucket's
	]
);
*/


//Retrieve the commit data, this is how bitbucket formats theirs
$data = isset($conig['json_decode']) ? json_decode($_POST[$conig['json_decode']], true) : $_POST;

//Setup git directory
$directory = dirname(dirname(__FILE__));

//Set the user config
$config['user'] = isset($config['user']) && isset($data[$config['user']]) ? $data[$config['user']] : null;
$config['mesage'] = isset($config['message']) && isset($data[$config['message']]) ? $data[$config['message']] : null;
$config['callback'] = function($d){

	function delTree($dir) {
		$dir = rtrim($dir,'/').'/';
		if($files = glob( $dir . '*', GLOB_MARK )){
			foreach( $files as $file ){
				if( substr( $file, -1 ) == '/' )
					delTree( $file );
				else
					unlink( $file );
			}
		}
		if(is_dir($dir)) rmdir( $dir );
	}

	delTree(dirname(dirname(__FILE__)).'/cache/database/');
	delTree(dirname(dirname(__FILE__)).'/cache/template/');

	//Clean apc
	if(extension_loaded('apc')){
		apc_clear_cache();
		apc_clear_cache('user');
		apc_clear_cache('opcode');
	}

	$d->log('Cleaned cache');
};


//Invoke deployment class
$deploy = new Deploy($directory,$config);

/**
Pass the repo name from the post data to validate it
Pass the commits from the post data to validate, only required if validate_branch is true
Commits must be either an array or an object containing 2 properties:
branch - the branch name
message - the message text

eg:
array(
	array('branch' => 'master', 'message' => 'my commit message')
)

The force flag (second arg to execute) allows for the process to be forced, use with caution
**/

//Execute deployment
$deploy->execute(array(
	'repo' => $data ? $data->repository->slug : null,
	'commits' => $data ? $data->commits : null
), (isset($_GET['force']) && $_GET['force']) ? true : false);