<?php

#date_default_timezone_set('America/Los_Angeles');

class Deploy
{

	/**
	 * A callback function to call after the deploy has finished.
	 *
	 * @var callback
	 */
	public $post_deploy;

	/**
	 * The name of the file that will be used for logging deployments. Set to
	 * FALSE to disable logging.
	 *
	 * @var string
	 */
	private $_log = 'deployments.log';

	/**
	 * The timestamp format used for logging.
	 *
	 * @link    http://www.php.net/manual/en/function.date.php
	 * @var     string
	 */
	private $_date_format = 'Y-m-d H:i:sP';

	/**
	 * @var
	 */
	private $_repo = '';

	/**
	 * The name of the branch to pull from.
	 *
	 * @var string
	 */
	private $_branch = 'master';

	/**
	 * Used in the validation function to validate the branch
	 * @var bool
	 */
	private $_validate_branch = false;

	/**
	 * The name of the remote to pull from.
	 *
	 * @var string
	 */
	private $_remote = 'origin';

	/**
	 * The directory where your website and git repository are located, can be
	 * a relative or absolute path
	 *
	 * @var string
	 */
	private $_directory;

	/**
	 * The host for the repo, enter a domain or IP address
	 * @var
	 */
	private $_hosts = array();
	
	/**
	 * Username submitting the push
	 * @var string
	 */
	private $_user = '';
	
	/**
	 * Log messages to screen
	 */
	private $_log_screen = true;

	/**
	 * Update submodules - set to force to do a pull on all subs
	 * @var bool
	 */
	private $_submodules = false;

	/**
	 * Message var
	 * @var
	 */
	private $_message;

	private $_callback = false;

	/**
	 * Sets up defaults.
	 *
	 * @param  string  $directory  Directory where your website is located
	 * @param  array   $data       Information about the deployment
	 */
	public function __construct($directory, $options = array())
	{
		foreach ($options as $option => $value) {
			if(isset($this->{'_' . $option})) $this->{'_' . $option} = $value;
		}

		// Determine the directory path
		$this->_directory = realpath($directory) . DIRECTORY_SEPARATOR;
		
		header('Content-type: text/plain');

		//Disabled output buffering
		for($i = 0; $i < 5000; $i++)
		{
			echo ' ';
		}

		@ob_implicit_flush(true);
		while (@ob_end_flush());
	}

	/**
	 * Writes a message to the log file.
	 *
	 * @param  string  $message  The message to write
	 * @param  string  $type     The type of log message (e.g. INFO, DEBUG, ERROR, etc.)
	 */
	public function log($message, $type = 'INFO')
	{
		// Format: time --- type: message
		$msg = date($this->_date_format) . ' --- ' . $type . ': ' . $message . PHP_EOL;
		
		if ($this->_log) {
			// Set the name of the log file
			$filename = dirname(__FILE__).'/'.$this->_log;

			if (!file_exists($filename)) {
				// Create the log file
				file_put_contents($filename, '');

				// Allow anyone to write to log files
				chmod($filename, 0666);
			}
			
			// Write the message into the log file			
			file_put_contents($filename, $msg, FILE_APPEND);
		}
		
		if($this->_log_screen){
			echo $msg;
			@flush();
			@ob_flush();
		}
	}


	/**
	 * @param $data
	 * @return bool
	 */
	public function validate($data)
	{
		//Check there is data
		if(!$data){
			$this->log('Error: deployment activated with no validation data.');
			return false;
		}

		//Check host
		if(!empty($this->_hosts))
		{
			$request_ip = $_SERVER['REMOTE_ADDR'];
			$request_host = gethostbyaddr($request_ip);
			$match = false;

			foreach($this->_hosts AS $host)
			{
				//Check if host is an IP
				if(preg_match('#[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}#', $host))
				{
					if($host == $request_ip) $match = true;
				}else{
					if($host == $request_host) $match = true;
				}
			}

			if(!$match)
			{
				$this->log('Error: the host making the request ('.$request_ip.' : '.$request_host.') does not match any of the config hosts: '.implode(',',$this->_hosts));
				return false;
			}
		}

		//Check for repo
		if($this->_repo && !isset($data['repo'])){
			$this->log('Error: no repo specified.');
			return false;
		}

		//Check repo name
		if($this->_repo && strtolower($data['repo']) != strtolower($this->_repo))
		{
			$this->log('Error: supplied repo ('.$data['repo'].') does not match config repo ('.$this->_repo.')');
			return false;
		}

		//Validate the branch matches
		if($this->_validate_branch && $this->_branch)
		{			
			//Commits can be an empty array if we're doing a revert
			if(empty($data['commits']))
			{
				$this->log('No commit messages found.');
				return true;
			}

			//Validate branch
			$has_branch = false;
			foreach($data['commits'] AS $commit)
			{
				$commit = (object) $commit;
				if($commit->branch == $this->_branch)
				{
					$has_branch = true;
					$this->log('Commit found: '.trim($commit->message));
				}
			}
			if($has_branch) return true;

			$this->log('Error: There are no commits for this branch');
			return false;
		}

		return true;
	}

	/**
	 * Executes the necessary commands to deploy the website.
	 */
	public function execute($data, $force = false)
	{
		$this->log('Attempting deployment...');
		if($this->_user) $this->log('Deployment by: '.$this->_user);
		if($this->_message) $this->log($this->_message);

		if ($force || $this->validate($data)){

			try {
				//Change working directory
				chdir($this->_directory);
				$this->log('Changing working directory to '.$this->_directory.' ... ');

				// Discard any changes to tracked files since our last deploy
				exec('git reset --hard HEAD', $output, $err);
				$this->log('Reseting repository... '."\n" . implode("\n", $output));
				unset($output);

				if($err == 127){
					$this->log('Unable to find git executable. Quitting.');
				}else{

					// Update the local repository
					exec('git pull ' . $this->_remote . ' ' . $this->_branch, $output);
					$this->log('Pulling in changes '."\n".$this->_remote . ' ' . $this->_branch.' ... ' . implode("\n", $output));
					unset($output);

					if($this->_submodules)
					{
						exec('git submodule foreach git reset --hard HEAD', $output);
						$this->log('Resetting all submodules '."\n" . implode("\n", $output));
						unset($output);

						//Update submodules
						exec('git submodule update --init --recursive', $output);
						$this->log('Updating submodules ... '."\n" . implode("\n", $output));
						unset($output);

						if($this->_submodules == 'force'){

							exec('git submodule foreach git checkout master', $output);
							$this->log('Checking out all submodules ... '."\n" . implode("\n", $output));
							unset($output);

							exec('git submodule foreach git pull', $output);
							$this->log('Pulling all submodules ... '."\n" . implode("\n", $output));
							unset($output);
						}
					}

					// Secure the .git directory
					exec('chmod -R og-rx .git');
					$this->log('Securing .git directory... ');

					if (is_callable($this->post_deploy)) {
						call_user_func($this->post_deploy, $this->_data);
					}

					$this->log('Deployment successful.');

					if($this->_callback && is_callable($this->_callback)){
						call_user_func($this->_callback, $this);
					}
				}
			} catch (Exception $e) {
				$this->log($e, 'ERROR');
			}
		}

		$this->log("End\n");
	}
}
